<?php

namespace App;

trait Done
{
    /**
     * @return int
     */
    public function done()
    {
        $query = $this->newQueryWithoutScopes()->where($this->getKeyName(), $this->getKey());
        $time = $this->freshTimestamp();

        $columns = ['completed_at' => $this->fromDateTime($time)];

        $this->completed_at = $time;

        if ($this->timestamps) {
            $this->{$this->getUpdatedAtColumn()} = $time;

            $columns[$this->getUpdatedAtColumn()] = $this->fromDateTime($time);
        }

        return $query->update($columns);
    }

    /**
     * @return bool
     */
    public function isDone()
    {
        return !is_null($this->completed_at);
    }

    /**
     * @return bool
     */
    public function undone()
    {
        // If the restoring event does not return false, we will proceed with this
        // restore operation. Otherwise, we bail out so the developer will stop
        // the restore totally. We will clear the deleted timestamp and save.
        if ($this->fireModelEvent('undone') === false) {
            return false;
        }

        $this->completed_at = null;

        // Once we have saved the model, we will fire the "restored" event so this
        // developer will do anything they need to after a restore operation is
        // totally finished. Then we will return the result of the save call.
        $this->exists = true;

        $result = $this->save();

        $this->fireModelEvent('undone', false);

        return $result;
    }

    /**
     * @param array $ids
     */
    public static function doneMany(array $ids)
    {
        $models = static::findMany($ids);

        foreach ($models as $model) {
            $model->done();
        }
    }

}
