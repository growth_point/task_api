<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Label
 *
 * @property int $id
 * @property string $title
 * @property int $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Task[] $tasks
 * @property-read \App\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Label onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Label whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Label whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Label whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Label whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Label whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Label whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Label withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Label withoutTrashed()
 * @mixin \Eloquent
 */
class Label extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public $timestamps = true;

    protected $fillable = ['title'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tasks()
    {
        return $this->belongsToMany('App\Task');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
