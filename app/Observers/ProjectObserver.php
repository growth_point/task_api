<?php

namespace App\Observers;

use App\Project;

class ProjectObserver
{
    /**
     * Listen to the Project creating event.
     *
     * @param  Project  $project
     * @return void
     */
    public function creating(Project $project)
    {
        if (!$project->user_id) {
            $project->user_id = request()->user->id;
        }
    }
}