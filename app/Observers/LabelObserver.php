<?php

namespace App\Observers;

use App\Label;

class LabelObserver
{
    /**
     * Listen to the User creating event.
     *
     * @param  Label  $label
     * @return void
     */
    public function creating(Label $label)
    {
        if (!$label->user_id) {
            $label->user_id = request()->user->id;
        }
    }
}