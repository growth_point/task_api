<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->unsignedInteger('project_id');
            $table->timestamp('completed_at', 0)->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('project_id')->references('id')->on('projects');
        });


        Schema::create('label_task', function (Blueprint $table) {
            $table->unsignedInteger('label_id');
            $table->unsignedInteger('task_id');

            $table->foreign('label_id')->references('id')->on('labels');
            $table->foreign('task_id')->references('id')->on('tasks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('label_task');
        Schema::dropIfExists('tasks');
        Schema::enableForeignKeyConstraints();
    }
}
