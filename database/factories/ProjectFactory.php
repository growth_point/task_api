<?php

use Faker\Generator as Faker;

$factory->define(App\Project::class, function (Faker $faker) {
    return [
        'title' => ucfirst($faker->unique()->words(3, true)),
    ];
});
