<?php

use Faker\Generator as Faker;

$factory->define(App\Label::class, function (Faker $faker) {
    return [
        'title' => ucfirst($faker->unique()->words(3, true)),
    ];
});

$factory->state(App\Label::class, 'today', [
    'title' => 'today',
]);