<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 20)->create()->each(function (App\User $u) {
            $today = factory(App\Label::class)->states('today')->make();
            $labels = $u->labels()->saveMany(factory(App\Label::class, 5)->make()->prepend($today));
            $projects = $u->projects()->saveMany(factory(App\Project::class, 5)->make());

            $projects->each(function (App\Project $p) use ($labels) {
                $p->tasks()->saveMany(factory(App\Task::class, 5)->make())->each(function (App\Task $t) use($labels) {
                    $t->labels()->sync($labels->random(3));
                });
            });
        });

    }
}