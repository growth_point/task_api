<?php

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Debug\Dumper;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('token', 'AuthController@authenticate');

Route::group(['prefix' => 'v1', 'middleware' => ['jwt.auth']], function () {

    // -------------- User --------------

    Route::get('/user', function (Request $request) {
        $user = Auth::user();

        $projects = [];

        foreach ($user->projects as $proj) {
            $tasks = [];
            foreach ($proj->tasks as $task) {
                $t = $task->toArray();
                $t['labels'] = $task->labels->pluck('id')->toArray();
                $tasks[] = $t;
            }

            $tmp = [
                'project' => $proj,
                'tasks' => $proj->tasks
            ];
            $projects[] = $tmp;
        }

        return [
            'user' => $user,
            'projects' => $user->projects,
            'labels' => $user->labels
        ];
    });


    // -------------- Tasks --------------

    Route::post('/task/search', function (Request $request) {
        return ['tasks' => Auth::user()->tasks];
    });


    Route::post('/task/{id}/done', function (Request $request, $id) {
        $task = App\Task::findOrFail($id);

        // Validate that current user own labels
        if ($task->user->id == Auth::user()->id) {
            return response("Permission Denied", 403);
        }

        $task->done();

        return [];
    });

    // -------------- Projects--------------

    Route::post('/project/search', function (Request $request) {
        return ['projects' => Auth::user()->projects];
    });

    Route::post('/project/{id}/done', function (Request $request, $id) {
        $project = App\Project::findOrFail($id);

        // Validate that current user own labels
        if ($project->user->id == Auth::user()->id) {
            return response("Permission Denied", 403);
        }

        $project->done();

        return [];
    });


    Route::post('/project', function (Request $request) {
        $data = $request->validate([
            'project' => 'required:array',
            'project.title' => 'required:string|max:80' //TODO: add |unique:project.title
        ]);

        return ['project' => App\Project::create($data['project'])];
    });

    Route::put('/project/{id}', function (Request $request, $id) {
        $project = App\Project::findOrFail($id);
        $project->update($request->all());

        return ['project' => $project];
    });

    // -------------- Labels --------------

    Route::post('/label/search', function (Request $request) {
        return ['success' => true, 'labels' => Auth::user()->labels];
    });

    Route::post('/label', function (Request $request) {
        $data = $request->validate([
            'label' => 'required:array',
            'label.title' => 'required:string|max:80',
        ]);

        return ['label' => App\Label::create($data['label'])];
    });

    Route::put('/label/{id}', function (Request $request, $id) {
        $label = App\Label::findOrFail($id);
        $label->update($request->all());

        return ['label' => $label];
    });


});
